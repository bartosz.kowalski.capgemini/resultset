package com.issuefix.mappers;

import com.issuefix.models.Parent;
import com.issuefix.dtos.ParentPageResultDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MapperConfig;
import org.mapstruct.ReportingPolicy;
import org.tkit.quarkus.jpa.daos.PageResult;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface ParentMapper {

    ParentPageResultDto mapToPageResultDto(PageResult<Parent> page);
}
