package com.issuefix.controllers;

import com.issuefix.mappers.ParentMapper;
import com.issuefix.dtos.ParentPageResultDto;
import com.issuefix.dtos.ParentSearchCriteriaDto;
import com.issuefix.controls.ParentService;
import lombok.NoArgsConstructor;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/results")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@NoArgsConstructor
public class ParentController {

    @Inject
    ParentService parentService;

    @Inject
    ParentMapper parentMapper;

    @POST
    public Response getResults(ParentSearchCriteriaDto parentSearchCriteriaDto) {
        ParentPageResultDto parentPageResultDto = parentMapper.mapToPageResultDto(parentService.findBySearchCriteriaPaged(parentSearchCriteriaDto));
        return Response.ok(parentPageResultDto).build();
    }
}