package com.issuefix.daos;

import com.issuefix.dtos.ParentSearchCriteriaDto;
import com.issuefix.models.Child_;
import com.issuefix.models.Parent;
import com.issuefix.models.Parent_;
import lombok.extern.slf4j.Slf4j;
import org.tkit.quarkus.jpa.daos.AbstractDAO;
import org.tkit.quarkus.jpa.daos.Page;
import org.tkit.quarkus.jpa.daos.PageResult;
import org.tkit.quarkus.jpa.utils.QueryCriteriaUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@ApplicationScoped
public class ParentDao extends AbstractDAO<Parent> {

    @Inject
    EntityManager em;

    public PageResult<Parent> findBySearchCriteriaPaged(ParentSearchCriteriaDto parentSearchCriteriaDto) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Parent> cq = cb.createQuery(Parent.class);
        Root<Parent> root = cq.from(Parent.class);
        cq.distinct(true);
        cq.where(cb.and(buildParentPredicates(cb, root, parentSearchCriteriaDto)));

        return createPageQuery(cq, Page.of(parentSearchCriteriaDto.getPageNumber(), parentSearchCriteriaDto.getPageSize())).getPageResult();
    }

    private Predicate[] buildParentPredicates(CriteriaBuilder cb, Root<Parent> root, ParentSearchCriteriaDto searchCriteriaDto) {
        List<Predicate> predicates = new ArrayList<>();

        if (isNotNullAndNotEmpty(searchCriteriaDto.getParentAttribute1())) {
            predicates.add(cb.equal(root.get(Parent_.PARENT_ATTRIBUTE1), searchCriteriaDto.getParentAttribute1()));
        }
        if (isListNotNullAndNotEmpty(searchCriteriaDto.getChildAttributes())) {
            predicates.add(QueryCriteriaUtil.inClause(root.join(Parent_.CHILDREN).get(Child_.CHILD_ATTRIBUTE),
                    searchCriteriaDto.getChildAttributes(), cb));
        }

        return predicates.toArray(new Predicate[0]);
    }

    public boolean isNotNullAndNotEmpty(String str) {
        return str != null && !str.isEmpty();
    }

    public boolean isListNotNullAndNotEmpty(List<Long> list) {
        return list != null && !list.isEmpty();
    }
}
