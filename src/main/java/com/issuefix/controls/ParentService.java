package com.issuefix.controls;

import com.issuefix.models.Parent;
import com.issuefix.daos.ParentDao;
import com.issuefix.dtos.ParentSearchCriteriaDto;
import lombok.extern.slf4j.Slf4j;
import org.tkit.quarkus.jpa.daos.PageResult;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class ParentService {

    @Inject
    ParentDao parentDao;

    public PageResult<Parent> findBySearchCriteriaPaged(ParentSearchCriteriaDto parentSearchCriteriaDto) {
        return parentDao.findBySearchCriteriaPaged(parentSearchCriteriaDto);
    }
}
