package com.issuefix.models;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = Child.TABLE_NAME)
public class Child extends TraceableEntity {

    public static final String TABLE_NAME = "CHILD_TABLE";

    @Column(name = "CHILD_ATTRIBUTE")
    private Long childAttribute;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_GUID", nullable = false, foreignKey = @ForeignKey(name = "FK_PARENT_GUID"))
    private Parent parent;
}
