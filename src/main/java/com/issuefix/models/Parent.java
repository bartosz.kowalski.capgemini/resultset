package com.issuefix.models;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = Parent.TABLE_NAME)
public class Parent extends TraceableEntity {

    public static final String TABLE_NAME = "PARENT_TABLE";

    @Column(name = "PARENT_ATTRIBUTE_1")
    private String parentAttribute1;

    @Column(name = "PARENT_ATTRIBUTE_2")
    private String parentAttribute2;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Set<Child> children = new HashSet<>();

    public Set<Long> getChildAttributes() {
        return children.stream().map(Child::getChildAttribute).collect(Collectors.toSet());
    }

    public void setChildAttributes(Set<Long> childAttributes) {
        children = new HashSet<>();
        childAttributes.forEach(this::setChildAttribute);
    }

    private void setChildAttribute(Long childAttribute) {
        Child child = new Child();
        child.setParent(this);
        child.setChildAttribute(childAttribute);
        children.add(child);
    }
}
