package com.issuefix.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ParentSearchCriteriaDto {

    private String parentAttribute1;

    private List<Long> childAttributes;

    /**
     * Index of the page.
     */
    @JsonAlias({ "page" })
    private Integer pageNumber = 0;

    /**
     * Size of the page
     */
    @JsonAlias({ "size" })
    private Integer pageSize = 500;
}
